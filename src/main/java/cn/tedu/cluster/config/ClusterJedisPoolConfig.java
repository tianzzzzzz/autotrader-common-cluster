package cn.tedu.cluster.config;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.pool2.impl.GenericObjectPoolConfig;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import redis.clients.jedis.HostAndPort;
import redis.clients.jedis.JedisCluster;

//将该类标记为
@Configuration
@ConfigurationProperties(prefix="spring.redis.cluster")
public class ClusterJedisPoolConfig {
	//configurationProperties注解,可以根据值
	//利用", ; "等隔开,分别add到同名的属性中
	private List<String> nodes;//["10.9.100.26:6379","",""]
	private Integer maxTotal;
	private Integer maxIdle;
	private Integer minIdle;
	@Bean
	public JedisCluster jClusterInit(){
		//利用属性完成连接池的初始化任务
		//第一步 收集节点信息
		Set<HostAndPort> set=new HashSet<HostAndPort>();
		for (String node : nodes) {
			//nodes=["10.9.100.26:6379","10.9.100.26:6380","10.9.100.26:6381"]
			//node="10.9.100.26:6379"
			String ip=node.split(":")[0];
			int port=Integer.parseInt(node.split(":")[1]);
			set.add(new HostAndPort(ip,port));
		}
		//封装一个config,设置最大连接数最大空闲
		GenericObjectPoolConfig config=new GenericObjectPoolConfig();
		config.setMaxTotal(maxTotal);
		config.setMaxIdle(maxIdle);
		config.setMinIdle(minIdle);
		return new JedisCluster(set, config);
	}
	public List<String> getNodes() {
		return nodes;
	}
	public void setNodes(List<String> nodes) {
		this.nodes = nodes;
	}
	public Integer getMaxTotal() {
		return maxTotal;
	}
	public void setMaxTotal(Integer maxTotal) {
		this.maxTotal = maxTotal;
	}
	public Integer getMaxIdle() {
		return maxIdle;
	}
	public void setMaxIdle(Integer maxIdle) {
		this.maxIdle = maxIdle;
	}
	public Integer getMinIdle() {
		return minIdle;
	}
	public void setMinIdle(Integer minIdle) {
		this.minIdle = minIdle;
	}
	
}